//
//  FutureViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 9/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import UIKit

@objc class FutureViewController: UIViewController {
  
  var conn : Connection = Connection()
  @IBOutlet weak var webView: UIWebView!
  
  @IBOutlet weak var btnChart: UIButton!
  var reply :NSString = "";
  @IBOutlet weak var RefreshSlider: UISlider!
  @IBOutlet weak var PositionAction: UISegmentedControl!
  @IBOutlet weak var ReverseSwitch: UISwitch!
  @IBOutlet weak var Threshold: UITextField!
    @IBOutlet weak var NetQty: UILabel!
  @IBOutlet weak var AlgoSwitch: UISwitch!
//  @IBOutlet weak var updateInterval: UILabel!
  @IBOutlet weak var SyncSwitch: UISwitch!
  @IBOutlet weak var nominalValue: UILabel!
  @IBOutlet weak var dayLow: UILabel!
  @IBOutlet weak var CommandPanel: UIView!
  @IBOutlet var CommandTap: UITapGestureRecognizer!
  @IBOutlet weak var dayHigh: UILabel!
  var refreshRate = 5;
  
    @IBOutlet weak var MCHvalue: UILabel!
  
 
  @IBOutlet weak var TradeItem: UILabel!
  @IBOutlet weak var PositionOption: UISegmentedControl!
  @IBOutlet weak var label1: UILabel!
  @IBOutlet weak var label2: UILabel!
  @IBOutlet weak var ActivePosition: UILabel!
//  var SwiftTimer = Timer()
  var keyboardDismissTapGesture: UIGestureRecognizer?
  
  func onRotate(_ notification: NSNotification) {
    if(UIDeviceOrientationIsLandscape(UIDevice.current.orientation))
    {
     // let url = NSURL(string: "http://tvccdn.investing.com/web/413/index57-prod.html?carrier=db4cc738bcf0af784850741258d9b7d7&time=1478686240&domain_ID=55&lang_ID=55&timezone_ID=28&version=413&locale=en&timezone=UTC&pair_ID=8984&interval=15&session=24x7&prefix=hk&suffix=&client=0&user=&family_prefix=tvc4&init_page=live-charts&sock_srv=stream96.forexpros.com&m_pids=")!
      //let url = NSURL(string: "http://hk.investing.com/charts/live-charts")!
      
      let url = NSURL(string: "http://tvc4.forexpros.com/init.php?family_prefix=tvc4&time=1479947567&domain_ID=55&lang_ID=55&timezone_ID=28&pair_ID=8984&interval=900&refresh=4&session=24x7&client=1&user=200995618&width=830&height=800&init_page=live-charts&m_pids=")!
      UIApplication.shared.open(url as URL)
    }
  }
  @IBOutlet var rotateTest: UIRotationGestureRecognizer!
  
  
    @IBAction func OpenPutBtn(_ sender: AnyObject) {
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_OPEN\",\"value\":\"SHORT\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    //  showAlert(reply)
    }
  
    @IBAction func SettlePutBtn(_ sender: AnyObject) {
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_SETTLE\",\"value\":\"FALSE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    //  showAlert(reply)
    }
    @IBAction func SettleCallBtn(_ sender: AnyObject) {
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_SETTLE\",\"value\":\"TRUE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    //  showAlert(reply)
    }
  
  @IBAction func OpenCallBtn(_ sender: UIButton) {
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_OPEN\",\"value\":\"LONG\",\"sync\":"+String(SyncSwitch.isOn)+"}")
     // showAlert(reply)
    }
  @IBAction func OpenDetailChart(_ sender: UIButton) {
    //let url1 = NSURL(string: "http://tvccdn.investing.com/web/413/index57-prod.html?carrier=db4cc738bcf0af784850741258d9b7d7&time=1478686240&domain_ID=55&lang_ID=55&timezone_ID=28&version=413&locale=en&timezone=UTC&pair_ID=8984&interval=15&session=24x7&prefix=hk&suffix=&client=0&user=&family_prefix=tvc4&init_page=live-charts&sock_srv=stream96.forexpros.com&m_pids=")!
    
    let url = NSURL(string: "http://hk.investing.com/charts/live-charts")!
    UIApplication.shared.open(url as URL)
  }
  
  
  @IBAction func UpdateRefreshRate(_ sender: UISlider) {
    refreshRate = Int(sender.value)
 //   updateInterval.text = String(refreshRate)
    CoreViewController.SwiftTimer?.invalidate()
    CoreViewController.SwiftTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(refreshRate), repeats: true) { timer in
      self.refresh()
    }
    
  }
  
  @IBAction func handleCommandTap(_ sender: UITapGestureRecognizer){
    if(CommandPanel.frame.origin.y<560){
      UIView.animate(withDuration: 0.2,
                     delay: 0.0,
                     animations: {
                      self.CommandPanel.frame.origin.y = 560
      })
    }else{
      UIView.animate(withDuration: 0.2,
                     delay: 0.0,
                     animations: {
                      self.CommandPanel.frame.origin.y = 420
      })
    }
    
  }
  @IBAction func OnReverseChange(_ sender: UISwitch) {
    if(sender.isOn){
     reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"REVERSE\",\"value\":\"TRUE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    }else{
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"REVERSE\",\"value\":\"FALSE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    }
   // showAlert(reply)
  }
  
  @IBAction func OnAlgoSwitchChange(_ sender: UISwitch) {
    if(!AlgoSwitch.isOn){
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_POSITION\",\"value\":\"NIL\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    }else{
      reset()
    }
   // showAlert(reply)
  }

  @IBAction func Refresh(_ sender: UIButton) {
    CoreViewController.SwiftTimer?.invalidate()
    CoreViewController.SwiftTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(refreshRate), repeats: true) { timer in
      self.refresh()
    }
    refresh()
  }
  
  @IBAction func Reset(_ sender: UIButton) {
    reset()
  }
  
  func reset(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_POSITION\",\"value\":\"NULL\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    if(reply.contains("executed")){
      PositionOption.selectedSegmentIndex = -1;
      PositionAction.selectedSegmentIndex = -1;
      
    }
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"OPEN_POSITION\",\"value\":\"-1\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    if(reply.contains("executed")){
      Threshold.text = ""
      
    }
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"REVERSE\",\"value\":\"FALSE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
    if(reply.contains("executed")){
      ReverseSwitch.setOn(false,animated: true)
      
    }
    
    //test/
   // let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChartViewController") as! ChartViewController
    
   // self.navigationController?.pushViewController(secondViewController, animated: true)
    //let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    
    //let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ChartViewController") as! ChartViewController
    //self.present(nextViewController, animated:true, completion:nil)
  }
  
  @IBAction func SetThreshold(_ sender: UITextField) {
    if(!sender.text!.isEmpty){
      reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"OPEN_POSITION\",\"value\":\""+sender.text!+"\",\"sync\":"+String(SyncSwitch.isOn)+"}")
     // showAlert(reply)
    }
  }
  
  @IBAction func HandlePosition(_ sender: UISegmentedControl) {
    var action : String

    if(PositionAction.selectedSegmentIndex > -1){
      if(PositionAction.selectedSegmentIndex==0){
        action = "FORCE_POSITION"
      }else{
        action = "FORCE_OPEN"
      }
      if(sender.selectedSegmentIndex==0){
        reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\""+action+"\",\"value\":\"LONG\",\"sync\":"+String(SyncSwitch.isOn)+"}")
      }else {
        reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\""+action+"\",\"value\":\"SHORT\",\"sync\":"+String(SyncSwitch.isOn)+"}")
      }
     //  showAlert(reply)
    }
  }
  
  @IBAction func OnPosActionChange(_ sender: UISegmentedControl) {
    PositionOption.selectedSegmentIndex = -1;
    
  }
  
  func refreshActiveOrder(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"ACTIVE_ORDER\",\"value\":\"TRUE\"}")
    print(reply )
    let str = reply as String
    if(str.contains("position")){
      let json = convertStringToDictionary(text: str)
      let position = json?["position"] as! String
      let orderPrice = json?["orderPrice"] as! Int
      ActivePosition.text = position + " " + String(orderPrice)
      if(position=="LONG"){
        ActivePosition.textColor=UIColor.green
      }else{
        ActivePosition.textColor=UIColor.red
      }
    }else{
      //update position label
      
      if(ActivePosition.text != "Position"){
         reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_POSITION\",\"value\":\"NULL\",\"sync\":"+String(SyncSwitch.isOn)+"}")
        if(reply.contains("executed")){
          PositionOption.selectedSegmentIndex = -1;
          PositionAction.selectedSegmentIndex = -1;
          Threshold.text = ""
        }
      }
      ActivePosition.text = "Position"
      ActivePosition.textColor=UIColor.white
      
      
    }
  }
  
  func refreshMarketData(){
   /* reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"MARKET_DATA\",\"value\":\"TRUE\"}")
    print(reply )
    let str = reply as String
   
      let json = convertStringToDictionary(text: str)
      //let open = json?["open"] as! Int
      let high = json?["high"] as! Int
      let low = json?["low"] as! Int
      let close = json?["close"] as! Int
      let prevClose = json?["prevClose"] as! Int

      
      if(close-prevClose>=0){
        nominalValue.textColor=UIColor.green
        nominalValue.text = String(close)+"  +"+String(close-prevClose)
      }else{
        nominalValue.textColor=UIColor.red
        nominalValue.text = String(close)+"  -"+String(prevClose-close)
      }*/
      nominalValue.textColor = SharingManager.sharedInstance.MCHnominalValue.textColor
      nominalValue.text = SharingManager.sharedInstance.MCHnominalValue.text
      dayHigh.text = SharingManager.sharedInstance.MCHdayHigh
      dayLow.text = SharingManager.sharedInstance.MCHdayLow
      NetQty.text = SharingManager.sharedInstance.NetQty
  }
  
  func refreshCommandPanel(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"REMOTE_CONFIGURATION\",\"value\":\"TRUE\"}")
    print(reply )
    let str = reply as String
    
    let json = convertStringToDictionary(text: str)
   // let forceSettle = json?["forceSettle"] as!Bool
    let breakPt = Int(json?["breakPt"] as!String)
    let reverse = Bool(json?["reverse"] as!String)
    let forcePosition = json?["forcePosition"] as!String
    let forceOpen = Bool(json?["forceOpen"] as!String)
    
    if(forcePosition=="LONG"||forcePosition=="SHORT"){
      if(forceOpen)!{
        PositionAction.selectedSegmentIndex=1
      }else{
        PositionAction.selectedSegmentIndex=0
      }
      if(forcePosition=="LONG"){
        PositionOption.selectedSegmentIndex=0

      }else{
        PositionOption.selectedSegmentIndex=1
      }
    }else if(forcePosition=="NIL"){
      AlgoSwitch.setOn(false, animated: true);
    }
    
    if(reverse)!{
      ReverseSwitch.setOn(true,animated: false)
    }
    
    if(breakPt! > -1){
      Threshold.text = String(breakPt!)
    }

  }
  
  func handleSwipes(sender:UISwipeGestureRecognizer) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if (sender.direction == .left) {
      
      let vc = storyboard.instantiateViewController(withIdentifier: "ChartViewController")
      self.present(vc, animated: false, completion: nil)

    }
    
    if (sender.direction == .right) {

      
    }
  }

  
  func refresh(){
    //loadChart()
    refreshActiveOrder()
    refreshMarketData()
    refreshCommandPanel()
  }
  
  func convertStringToDictionary(text: String) -> [String:AnyObject]? {
    if let data = text.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }
  
  @IBAction func ForceSettleBtn(_ sender: AnyObject) {
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"FORCE_SETTLE\",\"value\":\"TRUE\",\"sync\":"+String(SyncSwitch.isOn)+"}")
   // showAlert(reply)
  }
  
 
  
  func showAlert(_ msg: NSString){
    let alert = UIAlertController(title: "Done", message: msg as String , preferredStyle: UIAlertControllerStyle.alert)
   // alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  func loadChart(){
    let url = NSURL (string: "http://tvc4.forexpros.com/init.php?family_prefix=tvc4&time=1479947567&domain_ID=55&lang_ID=55&timezone_ID=28&pair_ID=8984&interval=900&refresh=4&session=24x7&client=1&user=200995618&width=830&height=800&init_page=live-charts&m_pids=");
  /*  let url2 = NSURL (string: "http://charts.aastocks.com/servlet/Charts?fontsize=12&15MinDelay=T&lang=1&titlestyle=1&vol=1&Indicator=9&indpara1=20&indpara2=1&indpara3=0&indpara4=0&indpara5=0&subChart2=3&ref2para1=12&ref2para2=26&ref2para3=9&subChart3=7&ref3para1=20&ref3para2=5&ref3para3=0&subChart4=2&ref4para1=14&ref4para2=0&ref4para3=0&scheme=3&com=100&chartwidth=380&chartheight=590&stockid=221000.HK&period=5000&type=1&AHFT=T&logoStyle=1&")*/
    let request = NSURLRequest(url: url! as URL);
    
       webView.loadRequest(request as URLRequest);
   
  }
  

  func keyboardWillShow(sender: NSNotification) {
    self.view.frame.origin.y = -200
    if keyboardDismissTapGesture == nil
    {
      keyboardDismissTapGesture = UITapGestureRecognizer(target: self, action: #selector(FutureViewController.dismissKeyboard(sender:)))
      self.view.addGestureRecognizer(keyboardDismissTapGesture!)
    }
  }
  
  func keyboardWillHide(sender: NSNotification) {
    self.view.frame.origin.y = 0
    if keyboardDismissTapGesture != nil
    {
      self.view.removeGestureRecognizer(keyboardDismissTapGesture!)
      keyboardDismissTapGesture = nil
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.setTitle(title : self.title!)
  //  self.dismiss(animated: true, completion: nil)
    
    if (SharingManager.sharedInstance.theme == "WHITE"){
      Globals.themeTextColor = UIColor.darkGray
    }else if (SharingManager.sharedInstance.theme == "DARK"){
      Globals.themeTextColor = UIColor.white
    }
    label1.textColor = Globals.themeTextColor
    label2.textColor = Globals.themeTextColor
    ActivePosition.textColor = Globals.themeTextColor
    dayHigh.textColor = Globals.themeTextColor
    dayLow.textColor = Globals.themeTextColor
    TradeItem.textColor = Globals.themeTextColor
    NetQty.textColor = Globals.themeTextColor
  }
  
  override func viewDidDisappear(_ animated: Bool) {
     //CoreViewController.SwiftTimer?.invalidate()
  }

  
    override func viewDidLoad() {
        super.viewDidLoad()
      
  /*    self.navigationController?.setNavigationBarHidden(true, animated: true)*/
      
      self.view.addBackground()
      
      
     
   //   self.performSegue(withIdentifier: "setTitle", sender: self)
      
      loadChart()
      //refreshCommandPanel()
      refresh()
      NotificationCenter.default.addObserver(self, selector: #selector(FutureViewController.onRotate(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
      //updateInterval.text = String(refreshRate)
      if(CoreViewController.TradeTimer==nil){
      CoreViewController.TradeTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(refreshRate), repeats: true) { timer in
        self.refresh()
      }
      }
      

      
      NotificationCenter.default.addObserver(self, selector: #selector(FutureViewController.keyboardWillShow(sender:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
      NotificationCenter.default.addObserver(self, selector: #selector(FutureViewController.keyboardWillHide(sender:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
      CommandTap.numberOfTapsRequired=2
      
      
     /* let leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
      let rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
      
      leftSwipe.direction = .left
      rightSwipe.direction = .right
      
      view.addGestureRecognizer(leftSwipe)
      view.addGestureRecognizer(rightSwipe)*/
self.hideKeyboardWhenTappedAround()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  func dismissKeyboard(sender: AnyObject) {
    Threshold?.resignFirstResponder()
  }
  
/*  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
   // let lobbyViewController : LobbyViewController = (segue.destination as? LobbyViewController)!
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    let cv = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController")
     if "setTitle" == segue.identifier {
      
      cv.tabBarController?.selectedIndex = 1
   //     coreViewController.coreTitle?.text = "Future"
    }
      //coreViewController.data = data
    
  }*/
  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
