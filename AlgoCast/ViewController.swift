//
//  ViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 4/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import UIKit
import CoreData

extension UIView {
  func addBackground(bg : String = "") {
    
    if let viewWithTag = self.viewWithTag(Globals.tagBG) {
      viewWithTag.removeFromSuperview()
    }
    
    // screen width and height:
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.height
    
    let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width:width, height:height))
    
    /*var bgimage : String = "algomainbg.png"
    if (bg == "login"){
      bgimage = "loginbg.png"
    }*/
    var bgimage : String = "mainbg_white.jpg"

    if (SharingManager.sharedInstance.theme == "WHITE"){
      bgimage = "mainbg_white.jpg"
    }else if (SharingManager.sharedInstance.theme == "DARK"){
      bgimage = "mainbg_dark.jpg"
    }
    
    imageViewBackground.image = UIImage(named: bgimage)
    
    // you can change the content mode:
    imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
    imageViewBackground.tag = Globals.tagBG
    self.isUserInteractionEnabled = true
    self.addSubview(imageViewBackground)
    self.sendSubview(toBack: imageViewBackground)
  }
  
  func setTitle(title : String=""){
    SharingManager.sharedInstance.pageTitle = title
   // self.dismiss(animated: true, completion: nil)
     let cv : CoreViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CoreViewController") as! CoreViewController
    
  cv.viewDidLoad()
  }

  func addHeader() {
    if let viewWithTag = self.viewWithTag(Globals.tagHeader) {
      viewWithTag.removeFromSuperview()
    }

    // screen width and height:
    let width = UIScreen.main.bounds.size.width
    let height = 120
    
    let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width:Int(width), height:height))
    
    var bgimage : String = "algoheader.png"
    
    if (SharingManager.sharedInstance.theme == "WHITE"){
      bgimage = "algoheader.png"
    }else if (SharingManager.sharedInstance.theme == "DARK"){
      bgimage = "algoheader_dark.png"
    }

    
    imageViewBackground.image = UIImage(named: bgimage)
    
    // you can change the content mode:
    imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
    imageViewBackground.tag = Globals.tagHeader
    self.isUserInteractionEnabled = true
    self.addSubview(imageViewBackground)
    self.sendSubview(toBack: imageViewBackground)
  }
  
  func getDate()-> String
  {
    let date = Date() // Get Todays Date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    let calendar = Calendar.current
    
    let hour = calendar.component(.hour, from: date)
    let minutes = calendar.component(.minute, from: date)
    let seconds = calendar.component(.second, from: date)
    let stringDate: String = dateFormatter.string(from: date as Date) + " " + String(hour)+":"+String(minutes )+":"+String(seconds)
    return stringDate
  }
}

// Put this piece of code anywhere you like
extension UIViewController {
  func hideKeyboardWhenTappedAround() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
    view.addGestureRecognizer(tap)
  }
  
  func dismissKeyboard() {
    view.endEditing(true)
  }
  
/*  override func shouldAutorotate() -> Bool {
    return false
  }
  
  override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }*/
}

extension UINavigationController {
  override open var shouldAutorotate: Bool {
    return false
  }
}



class ViewController: UIViewController {
  
  @IBOutlet weak var userIDTextField: UITextField!
  @IBOutlet weak var userPasswordTextField: UITextField!
//  @IBOutlet weak var remotePort: UITextField!
  var records: [NSManagedObject] = []
  var saved : Bool = false
  var conn : Connection = Connection()

  @IBOutlet weak var btnSettings: UIButton!
  @IBOutlet weak var btnForgetPwd: UIButton!
  @IBOutlet weak var btnDisclaimer: UIButton!
  
  @IBAction func btnLogin(_ sender: AnyObject) {
    
    let remotePort : String = Globals.remotePort
    
    UserInfo.userid = userIDTextField.text!
    UserInfo.pwd = userPasswordTextField.text!
    Connection.serverPort = UInt32(remotePort)!//UInt32(remotePort.text!)!
    UserDefaults.standard.set(UserInfo.userid, forKey: "userid")
    UserDefaults.standard.set(remotePort, forKey: "port")
    
    if verifyCredentials(){
      
      
      let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ParentViewController")
      UIApplication.shared.keyWindow?.rootViewController = viewController
      
  //  let nextView : LobbyViewController = self.storyboard?.instantiateViewController(withIdentifier: "LobbyViewController") as! LobbyViewController
    
   // let appdelegate = UIApplication.shared.delegate as! AppDelegate
      
      
    
    //appdelegate.window!.rootViewController = nextView
    }else{
      // create the alert
      let alert = UIAlertController(title: Globals.ApplicationName, message: "Incorrent Login/Password", preferredStyle: UIAlertControllerStyle.alert)
      
      // add an action (button)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
      
      // show the alert
      self.present(alert, animated: true, completion: nil)
      
      userIDTextField.resignFirstResponder()
      userPasswordTextField.resignFirstResponder()
    }
    if(UserDefaults.standard.object(forKey: "deviceToken") != nil){
      let deviceToken = UserDefaults.standard.object(forKey: "deviceToken") as! String
      conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"DEVICE_TOKEN\",\"value\":\""+deviceToken+"\"}")
    }
    
  }
  
 
  
  @IBOutlet var btnLogin: [UIButton]!
  
  
  override func viewDidLoad() {
  
    super.viewDidLoad()
    
    self.view.addBackground(bg: "login")
    
  
    // Do any additional setup after loading the view, typically from a nib.
    let user = UserDefaults.standard.object(forKey: "userid") as? String
    //let port = UserDefaults.standard.object(forKey: "port") as? String
    userIDTextField.text = user
//    remotePort.text = "5987" //port
    
    
 
    let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
    let components = calendar?.components([.hour, .minute, .second], from: NSDate() as Date)
    let hour = components?.hour
    let minute = components?.minute
    
    if (hour == 23 && minute! > 45 && !saved){
      self.save()
    }
    
    
    self.hideKeyboardWhenTappedAround()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    //window?.tintColor = UIColor.redColor()
    if (SharingManager.sharedInstance.theme == "WHITE"){
      btnSettings.setImage(UIImage(named: "settings.png"), for: UIControlState.normal)
      Globals.themeTextColor = UIColor .darkGray
    }else if (SharingManager.sharedInstance.theme == "DARK"){
      btnSettings.setImage(UIImage(named: "settings_white.png"), for: UIControlState.normal)
      Globals.themeTextColor = UIColor .white
    }
    
    btnForgetPwd.setTitleColor(Globals.themeTextColor, for: UIControlState.normal)
    btnDisclaimer.setTitleColor(Globals.themeTextColor, for: UIControlState.normal)
  }
  


  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    
    // remove the func from here
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }

  
  func save() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    
    let managedContext = appDelegate.persistentContainer.viewContext
    
    let entity = NSEntityDescription.entity(forEntityName: "Record",
                                            in: managedContext)!
    
    let record = NSManagedObject(entity: entity,
                                 insertInto: managedContext)
    
    record.setValue(NSDate(), forKeyPath: "datetime")
    record.setValue(Globals.currentNetgain, forKeyPath: "netgain")
    record.setValue(Globals.currentProfit, forKeyPath: "profit")
    record.setValue("MCH", forKeyPath: "id")
    
    do {
      try managedContext.save()
      records.append(record)
      saved = true
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }

  
  func verifyCredentials() -> Bool
  {
    
    return true;
    
  }
  

  

/*    @IBAction func btnSkip(_ sender: AnyObject) {
        let nextView : PodcastFeedTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "PodcastFeedTableViewController") as! PodcastFeedTableViewController
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        appdelegate.window!.rootViewController = nextView
    }*/
  
  
  
}

