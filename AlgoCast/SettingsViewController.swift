//
//  BalanceViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 12/11/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//


import Foundation
import UIKit

@objc class SettingsViewController: UIViewController {
  @IBOutlet weak var Save: UIButton!
  @IBOutlet weak var remoteServer: UITextField!
  @IBOutlet weak var remotePort: UITextField!
  
  @IBOutlet weak var label1: UILabel!
  @IBOutlet weak var label2: UILabel!
  @IBOutlet weak var label3: UILabel!
  @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var themeSelect: UISegmentedControl!
    
    @IBAction func themeSwitchAction(_ sender: Any) {
      
      if(themeSelect.selectedSegmentIndex > -1){
        if(themeSelect.selectedSegmentIndex==0){
          SharingManager.sharedInstance.theme  = "WHITE"
        }else{
          SharingManager.sharedInstance.theme  = "DARK"
        }
      }
      self.view.addBackground()
          self.reloadTheme()
    }
    
    
  @IBAction func btnSave(_ sender: AnyObject) {
    let port = remotePort.text!
    let server = remoteServer.text!
    Connection.serverPort = UInt32(port)!
    Connection.serverAddress = server as CFString
    Globals.remotePort = port as String
    Globals.remoteServer = server as String
    UserDefaults.standard.set(port, forKey: "port")
    UserDefaults.standard.set(server, forKey: "serverAddress")
 /*   let nextView : LobbyViewController = self.storyboard?.instantiateViewController(withIdentifier: "LobbyViewController") as! LobbyViewController
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate */
    
   // appdelegate.window!.rootViewController = nextView
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    let port = UserDefaults.standard.object(forKey: "port") as? String
    let server = UserDefaults.standard.object(forKey: "serverAddress") as? String
    remotePort.text = Globals.remotePort
    remoteServer.text = Globals.remoteServer
    if((port != nil)){
      remotePort.text = port
    }
    if(server != nil){
      remoteServer.text = server
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    
     self.view.addBackground()
    
    self.hideKeyboardWhenTappedAround()
    
   
    self.reloadTheme()
  }
  
  func reloadTheme(){
    if (SharingManager.sharedInstance.theme == "WHITE"){
      themeSelect.selectedSegmentIndex=0
      Globals.themeTextColor = UIColor.darkGray
    }else if (SharingManager.sharedInstance.theme == "DARK"){
      themeSelect.selectedSegmentIndex=1
      Globals.themeTextColor = UIColor.white
    }
    
    label1.textColor = Globals.themeTextColor
    label2.textColor = Globals.themeTextColor
    label3.textColor = Globals.themeTextColor
    label4.textColor = Globals.themeTextColor
  }
}
