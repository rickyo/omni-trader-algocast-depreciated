//
//  CoreViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 24/12/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import Foundation
import UIKit

class CoreViewController: UIViewController{
  
  @IBOutlet weak var AppMenu: UIImageView!
    @IBOutlet weak var MCHnominalValue: UILabel!
  var refreshRate = 3;
  @IBOutlet weak var coreTitle: UILabel! 
  static var  SwiftTimer : Timer?=nil
  static var  TradeTimer : Timer?=nil
  static var  BalanceTimer : Timer?=nil

  static var updateTimer: Timer?=nil
  var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
 // var data : Data!
  
  var conn : Connection =  Connection()
    var reply :NSString = "";
  
  func convertStringToDictionary(text: String) -> [String:AnyObject]? {
    if let data = text.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }
  
  
  func refreshMarketData(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"MARKET_DATA\",\"value\":\"TRUE\"}")
    print("["+self.view.getDate()+"]:"+(reply as String))
    let str = reply as String
    
    let json = convertStringToDictionary(text: str)
    //let open = json?["open"] as! Int
    let high =  Int(Double(json?["high"] as! String)!)
    let low = Int(Double(json?["low"] as! String)!)
    let close = Int(Double(json?["close"] as! String)!)
    let prevClose = Int(Double(json?["prevClose"] as! String)!)
    let netQty = json?["netQty"] as! String
    
    
    
    if(close-prevClose>=0){
      SharingManager.sharedInstance.MCHnominalValue.textColor=UIColor.green
      SharingManager.sharedInstance.MCHnominalValue.text = String(close)+"  +"+String(close-prevClose)
    }else{
      SharingManager.sharedInstance.MCHnominalValue.textColor=UIColor.red
      SharingManager.sharedInstance.MCHnominalValue.text = String(close)+"  -"+String(prevClose-close)
    }
    SharingManager.sharedInstance.MCHdayHigh = String(high)
    SharingManager.sharedInstance.MCHdayLow = String(low)
    SharingManager.sharedInstance.NetQty = netQty
     
    MCHnominalValue.textColor = SharingManager.sharedInstance.MCHnominalValue.textColor
    MCHnominalValue.text = SharingManager.sharedInstance.MCHnominalValue.text
  }
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.setTitle(title : "Future")
       self.coreTitle.becomeFirstResponder()
 //   self.dismiss(animated: true, completion: nil)
   
    if(CoreViewController.updateTimer==nil){
      CoreViewController.updateTimer = Timer.scheduledTimer(timeInterval: TimeInterval(refreshRate), target: self,
                                       selector: #selector(refreshComponent), userInfo: nil, repeats: true)
      registerBackgroundTask()
      
      if (SharingManager.sharedInstance.theme == "WHITE"){
        Globals.themeTextColor = UIColor.darkGray
      }else if (SharingManager.sharedInstance.theme == "DARK"){
        Globals.themeTextColor = UIColor.white
      }

      coreTitle.textColor = Globals.themeTextColor
    }
    
    
    
   
  }
  
  func refreshComponent()
  {
    self.refreshMarketData()
    self.coreTitle.text = SharingManager.sharedInstance.pageTitle
    /*switch UIApplication.shared.applicationState {
    case .active:

    case .background:
        
      print("background")
    case .inactive:
      break
    }*/
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    CoreViewController.SwiftTimer?.invalidate()
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.addHeader()
    
    self.coreTitle.text = SharingManager.sharedInstance.pageTitle
    self.coreTitle.becomeFirstResponder()
    /*    self.navigationController?.setNavigationBarHidden(true, animated: true)*/

   // NotificationCenter.default.addObserver(self, selector: #selector(FutureViewController.onRotate(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    //updateInterval.text = String(refreshRate)
    
    refreshMarketData()
    // NotificationCenter.default.addObserver(self, selector: #selector(FutureViewController.onRotate(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    //updateInterval.text = String(refreshRate)
    if(CoreViewController.SwiftTimer==nil){
      CoreViewController.SwiftTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(refreshRate), repeats: true) { timer in
      self.refreshMarketData()
      
      self.coreTitle.text = SharingManager.sharedInstance.pageTitle
    }
    }
    /*NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
   */
 
    
 /*   concurrentQueues()
    if let queue = inactiveQueue {
      queue.activate()
    }*/
    
    }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  func reinstateBackgroundTask() {
    if CoreViewController.updateTimer != nil && (backgroundTask == UIBackgroundTaskInvalid) {
      registerBackgroundTask()
    }
  }
  
  func registerBackgroundTask() {
    backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
      self?.endBackgroundTask()
    }
    assert(backgroundTask != UIBackgroundTaskInvalid)
  }
  
  func endBackgroundTask() {
    print("Background task ended.")
    UIApplication.shared.endBackgroundTask(backgroundTask)
    backgroundTask = UIBackgroundTaskInvalid
  }



  
  
 

}
