//
//  GraphViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 16/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import Foundation
import UIKit

class ChartViewController: UIViewController {
  
  @IBOutlet weak var webview: UIWebView!
 
  func loadChart(){
  /*  let url = NSURL (string: "http://charts.aastocks.com/servlet/Charts?fontsize=12&15MinDelay=T&lang=1&titlestyle=1&vol=1&Indicator=9&indpara1=20&indpara2=1&indpara3=0&indpara4=0&indpara5=0&subChart1=2&ref1para1=14&ref1para2=0&ref1para3=0&subChart2=3&ref2para1=12&ref2para2=26&ref2para3=9&subChart5=7&ref5para1=20&ref5para2=5&ref5para3=0&scheme=3&com=100&chartwidth=373&chartheight=590&stockid=221000.HK&period=5000&type=1&AHFT=T&logoStyle=1&");*/
    let url = NSURL (string: "http://tvc4.forexpros.com/init.php?family_prefix=tvc4&time=1479947567&domain_ID=55&lang_ID=55&timezone_ID=28&pair_ID=8984&interval=900&refresh=4&session=24x7&client=1&user=200995618&width=830&height=800&init_page=live-charts&m_pids=");
    
    let request = NSURLRequest(url: url! as URL);
    webview.loadRequest(request as URLRequest);
  }
  
  override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
    self.view.setTitle(title : "Chart")
  }
  
  override func viewDidLoad() {
    
    //
    super.viewDidLoad()
     self.view.addBackground()
    
    
   
    loadChart()
    
    
 /*   let leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    let rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    
    leftSwipe.direction = .left
    rightSwipe.direction = .right
    
    view.addGestureRecognizer(leftSwipe)
    view.addGestureRecognizer(rightSwipe) */

    
    
  }
  
/*  func handleSwipes(sender:UISwipeGestureRecognizer) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)

    if (sender.direction == .left) {
      let vc = storyboard.instantiateViewController(withIdentifier: "FutureViewController")
      self.present(vc, animated: false, completion: nil)
      
    }
    
    if (sender.direction == .right) {
      
      let vc = storyboard.instantiateViewController(withIdentifier: "RecordViewController")
      self.present(vc, animated: false, completion: nil)
    }
    
  }*/
}
