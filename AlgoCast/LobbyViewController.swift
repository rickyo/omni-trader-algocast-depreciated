//
//  LobbyViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 9/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import UIKit

/*
extension LobbyViewController: UITabBarControllerDelegate {
  public func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
    
    let fromView: UIView = tabBarController.selectedViewController!.view
    let toView  : UIView = viewController.view
    if fromView == toView {
      return false
    }
    
    UIView.transition(from: fromView, to: toView, duration: 0.3, options: UIViewAnimationOptions.transitionCrossDissolve) { (finished:Bool) in
      
    }
    return true
  }
}*/


class LobbyViewController: UIViewController, UITabBarDelegate {
  
    @IBOutlet weak var LogoutBtn: UIButton!
   
    @IBOutlet weak var AppTitle: UILabel!
  
   @IBAction func OnFutureTouched(_ sender: Any) {
 //   let  navController = self.tabBarController?.viewControllers![1] as! UINavigationController
    ///secondviewcontroller in your case is cart
//    let futureViewController = navController.viewControllers[0] as! FutureViewController
    //set values you want to pass
    //lets say I want to pass name to secondVC
   // futureViewController.name = "ABCD"
    
    self.tabBarController?.selectedIndex = 1
    }
  
    @IBAction func OnRecordTouched(_ sender: Any) {
      
      self.tabBarController?.selectedIndex = 3
  }
    @IBAction func OnAccountTouched(_ sender: Any) {
      
      self.tabBarController?.selectedIndex = 4
    }
  
    @IBAction func OnChartTouched(_ sender: Any) {
      
      self.tabBarController?.selectedIndex = 2
    }
/*  override func loadView() {
    let vc1 : FutureViewController = self.storyboard?.instantiateViewController(withIdentifier: "FutureViewController") as! FutureViewController
    let nav1 = UINavigationController(rootViewController:vc1)
    let image1 = UIImage(named:"tab01.png")
    nav1.tabBarItem = UITabBarItem(title:"Trade", image:image1, tag:1)
    
/*    let vc2 : ChartViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChartViewController") as! ChartViewController
    let nav2 = UINavigationController(rootViewController:vc2)
    let image2 = UIImage(named:"tab03.png")
    nav2.tabBarItem = UITabBarItem(title:"Chart", image:image2, tag:2)*/
    
/*     let vc3 : RecordViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as! RecordViewController
    let nav3 = UINavigationController(rootViewController:vc3)
    let image3 = UIImage(named:"tab02.png")
    nav3.tabBarItem = UITabBarItem(title:"Record", image:image3, tag:3)*/
    
    let arr = [nav1]
    let tabBarController = UITabBarController()
    tabBarController.viewControllers = arr
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
   // appdelegate.window!.rootViewController = tabBarController
    
    
    
   }*/
  
  
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      
      self.view.addBackground()
   /*
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let mainVC = storyboard.instantiateViewController(withIdentifier: "FutureViewController")
      UIView.transition(with: self.view!, duration: 0.5, options:.transitionFlipFromLeft, animations: { () -> Void in
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController =  mainVC
      }, completion:nil)
      */

      
      AppTitle.text = Globals.ApplicationName

    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.setTitle(title : Globals.ApplicationName)
//    self.dismiss(animated: true, completion: nil)
    
  }
  


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LogoutAction(_ sender: Any) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
