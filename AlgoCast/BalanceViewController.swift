 //
//  BalanceViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 12/11/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import Foundation


import UIKit

@objc class BalanceViewController: UIViewController {
  @IBOutlet weak var NetGain: UILabel!
  
  var conn : Connection = Connection()
  var reply :NSString = "";
    @IBOutlet weak var AssetValue: UILabel!
  
    @IBOutlet weak var Cash: UILabel!
 
  @IBOutlet weak var TotalAmount: UILabel!
  @IBOutlet weak var NetAmount: UILabel!
  @IBOutlet weak var TotalGain: UILabel!
  @IBOutlet weak var label1: UILabel!
  @IBOutlet weak var label2: UILabel!
  @IBOutlet weak var label3: UILabel!
  @IBOutlet weak var label4: UILabel!
  func refresh(){
    refreshBalance()
  }

  
  func convertStringToDictionary(text: String) -> Array<[String:AnyObject]>? {
    if let data = text.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? Array<[String:AnyObject]>
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }

  func numberFormat(number : Decimal) -> String{
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .currency
    numberFormatter.locale = Locale.current
    numberFormatter.locale = Locale(identifier: "en_US")
    return numberFormatter.string(from: NSDecimalNumber(decimal: number))!
  }
  
  func refreshBalance(){
    
    let marginPoint : Double = 50
    var open_status : Bool = false
    var Netgain : String = "0"
  //  var CurrentProfitGain : String = "0"
    
    let marginAsset : Double = Globals.marginHHI
    
    
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"BALANCE\",\"value\":\"TRUE\"}")
    var profit : Double = 0
    if (reply != ""){
      try profit = Double(self.reply as String)!
      Netgain = reply as String
    }
    
    
    let marginProfit : Double = profit*marginPoint
    
    // check any open
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"OPEN_ORDER\",\"value\":\"TRUE\"}")
    
    let str = reply as String
    let json = self.convertStringToDictionary(text: str)
    print(str)
    print(json)
    if (json == nil){
    open_status = false
    }else
    if (str.contains("orderList") && (json?.count)! > 0){
      open_status = true
    }
   
if (profit != 0)
{
  
    if(profit>0){
        NetGain.textColor=UIColor.green
        NetGain.text = "+"+Netgain
        
        NetAmount.textColor=UIColor.green
        NetAmount.text = "+"+numberFormat(number: Decimal(marginProfit))
        
    }else{
        NetGain.textColor=UIColor.red
        NetGain.text = (Netgain as String)
      
        NetAmount.textColor=UIColor.red
        NetAmount.text = "(-"+numberFormat(number: Decimal(abs(marginProfit)))+")"
    }
}else{
  NetGain.textColor=Globals.themeTextColor
  NetGain.text = ""
  
  NetAmount.textColor=Globals.themeTextColor
  NetAmount.text = numberFormat(number: Decimal(0))
  
    }
    var accprofit : Double = profit + 50
    
    
    if(accprofit>0){
        TotalGain.textColor=UIColor.green
        TotalGain.text = "+"+String(accprofit)
        
        
    }else{
        TotalGain.textColor=UIColor.red
        TotalGain.text = String(accprofit)
        
        
    }
    
    TotalAmount.textColor=Globals.themeTextColor
    TotalAmount.text = numberFormat(number: Decimal(marginProfit + Globals.currentTotalProfit))
    

    if (open_status)
    {
           AssetValue.text = numberFormat(number: Decimal(marginAsset + marginProfit))
      
      Cash.text = numberFormat(number: Decimal(Globals.cash + marginProfit))
      
    }else{
      Cash.text = numberFormat(number: Decimal(Globals.cash + marginProfit))
      
      AssetValue.text = numberFormat(number: Decimal(0))
    }
    
    
    
    
    
  }
  
  func handleSwipes(sender:UISwipeGestureRecognizer) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    if (sender.direction == .left) {
      let vc = storyboard.instantiateViewController(withIdentifier: "RecordViewController")
      self.present(vc, animated: false, completion: nil)
      
    }
    
    if (sender.direction == .right) {
      
    }
  }

  
  override func viewDidDisappear(_ animated: Bool) {
    CoreViewController.BalanceTimer?.invalidate()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.setTitle(title : self.title!)

   // self.dismiss(animated: true, completion: nil)
    
    
   if (SharingManager.sharedInstance.theme == "WHITE"){
   Globals.themeTextColor = UIColor.darkGray
  }else if (SharingManager.sharedInstance.theme == "DARK"){
   Globals.themeTextColor = UIColor.white
  
  }
    
    
    label1.textColor=Globals.themeTextColor
    label2.textColor=Globals.themeTextColor
    label3.textColor=Globals.themeTextColor
    label4.textColor=Globals.themeTextColor
    
    Cash.textColor=Globals.themeTextColor
    AssetValue.textColor=Globals.themeTextColor
    NetAmount.textColor=Globals.themeTextColor
    NetGain.textColor=Globals.themeTextColor
    TotalAmount.textColor=Globals.themeTextColor
    TotalGain.textColor=Globals.themeTextColor
    
    refreshBalance()
  
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.addBackground()
    
   // self.view.setTitle(title : "Balance")
    
    
/*    let leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    let rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    
    leftSwipe.direction = .left
    rightSwipe.direction = .right
    
    view.addGestureRecognizer(leftSwipe)
    view.addGestureRecognizer(rightSwipe)*/


    refresh()
    if(CoreViewController.BalanceTimer==nil){
    CoreViewController.BalanceTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(2), repeats: true) { timer in
      self.refresh()
    }
    }
  }
}
