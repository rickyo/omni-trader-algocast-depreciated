//
//  RecordViewController.swift
//  AlgoCast
//
//  Created by RickyoKita on 12/11/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@objc class RecordViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
  var conn : Connection = Connection()
    @IBOutlet weak var RecordView: UITableView!
  var reply :NSString = ""
  var items: [String] = []
  
  
  var records = [NSManagedObject]()
  
  var filter :NSString = "All" //default
  
    @IBOutlet weak var btnRecSettled: UIButton!
    @IBOutlet weak var btnRecPending: UIButton!
    @IBOutlet weak var btnRecOpen: UIButton!
    @IBOutlet weak var btnRecAll: UIButton!
  @IBAction func btnRecordSettled(_ sender: UIButton, forEvent event: UIEvent) {
    filter = "Settled"
    reloadView()
    self.RecordView.reloadData()
    self.refreshButton(button: "Settled")
  }
  @IBAction func btnRecordPending(_ sender: UIButton, forEvent event: UIEvent) {
    filter = "Pending"
    reloadView()
    self.RecordView.reloadData()
    self.refreshButton(button: "Pending")
  }
  @IBAction func btnRecordOpen(_ sender: UIButton, forEvent event: UIEvent) {
    filter = "Open"
    reloadView()
    self.RecordView.reloadData()
    self.refreshButton(button: "Open")
  }
  
  @IBAction func btnRecordAll(_ sender: UIButton, forEvent event: UIEvent) {
    filter = "All"
    reloadView()
    self.RecordView.reloadData()
    self.refreshButton(button: "All")
  }
  

  
  func refreshButton(button : NSString)
  {
    
    btnRecAll.setTitleColor(self.view.tintColor, for: .normal)
    btnRecAll.backgroundColor = self.view.tintColor.withAlphaComponent(0.0)
    btnRecOpen.setTitleColor(self.view.tintColor, for: .normal)
    btnRecOpen.backgroundColor = self.view.tintColor.withAlphaComponent(0.0)
    btnRecSettled.setTitleColor(self.view.tintColor, for: .normal)
    btnRecSettled.backgroundColor = self.view.tintColor.withAlphaComponent(0.0)
    btnRecPending.setTitleColor(self.view.tintColor, for: .normal)
    btnRecPending.backgroundColor = self.view.tintColor.withAlphaComponent(0.0)

    
    if (button == "All"){
      btnRecAll.setTitleColor(UIColor.white, for: .normal)
      btnRecAll.backgroundColor = self.view.tintColor.withAlphaComponent(1.0)
    }
    if (button == "Open"){
      btnRecOpen.setTitleColor(UIColor.white, for: .normal)
      btnRecOpen.backgroundColor = self.view.tintColor.withAlphaComponent(1.0)
    }
    if (button == "Settled"){
      btnRecSettled.setTitleColor(UIColor.white, for: .normal)
      btnRecSettled.backgroundColor = self.view.tintColor.withAlphaComponent(1.0)
    }    
    if (button == "Pending"){
      btnRecPending.setTitleColor(UIColor.white, for: .normal)
      btnRecPending.backgroundColor = self.view.tintColor.withAlphaComponent(1.0)
    }
    
  }
  
  func loadSettledOrder(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"SETTLED_ORDER\",\"value\":\"TRUE\"}")
    let str = reply as String
    let json = convertStringToDictionary(text: str)
    if(str.contains("orderList")){
      for item in json!{
        let list = item["orderList"] as! Array<[String:AnyObject]>
        let price = list[0]["orderPrice"] as! NSNumber
        let value = String(price.doubleValue)
        let position = list[0]["position"] as! String
        let pid = list[0]["id"] as! String
        items.append("(Settled) "+position+" "+value + " "+pid)
      }
    }
  }
  
  func loadOpenOrder(){
    reply = conn.sendCommand(command: "{\"account\":\""+UserInfo.userid+"\",\"action\":\"OPEN_ORDER\",\"value\":\"TRUE\"}")
    let str = reply as String
    let json = convertStringToDictionary(text: str)
    if(str.contains("orderList")){
      for item in json!{
        let list = item["orderList"] as! Array<[String:AnyObject]>
        let price = list[0]["orderPrice"] as! NSNumber
        let value = String(price.doubleValue)
        let position = list[0]["position"] as! String
        let pid = list[0]["id"] as! String
        items.append("(OPEN) "+position+" "+value + " " + pid)
      }
    }
  }
  
  func showAlert(_ msg: NSString){
    let alert = UIAlertController(title: "Done", message: msg as String , preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
    @IBAction func GetSettledOrder(_ sender: Any) {
        loadSettledOrder()
    }
  func convertStringToDictionary(text: String) -> Array<[String:AnyObject]>? {
    if let data = text.data(using: String.Encoding.utf8) {
      do {
        return try JSONSerialization.jsonObject(with: data, options: []) as? Array<[String:AnyObject]>
      } catch let error as NSError {
        print(error)
      }
    }
    return nil
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.setTitle(title : self.title!)
  //  self.dismiss(animated: true, completion: nil)
    
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.addBackground()
    
    //1
  //  let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
   // let managedContext = appDelegatmanagedObjectModelxt
    
    //2
//    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Record")
    
   /* let leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    let rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipes:")))
    
    leftSwipe.direction = .left
    rightSwipe.direction = .right
    
    view.addGestureRecognizer(leftSwipe)
    view.addGestureRecognizer(rightSwipe)*/
    
    reloadView()
    
  }
  
  func reloadView()
  {
    items = []
    if (filter == "All"){
      loadOpenOrder()
      loadSettledOrder()
    }
    
    if (filter == "Open"){
      loadOpenOrder()
    }
    if (filter == "Settled"){
      loadSettledOrder()
    }
    
    RecordView.register(UITableViewCell.self, forCellReuseIdentifier: "TextCell")
    RecordView.delegate = self
    RecordView.dataSource = self
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
    let row = indexPath.row
    cell.textLabel?.text = items[row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = items[indexPath.row]
    print(item)
  }
  
  func handleSwipes(sender:UISwipeGestureRecognizer) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    if (sender.direction == .left) {
      let vc = storyboard.instantiateViewController(withIdentifier: "ChartViewController")
      self.present(vc, animated: false, completion: nil)
      
    }
    
    if (sender.direction == .right) {
      
      let vc = storyboard.instantiateViewController(withIdentifier: "BalanceViewController")
      self.present(vc, animated: false, completion: nil)
    }
  }
  
  

}
