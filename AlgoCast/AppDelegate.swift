//
//  AppDelegate.swift
//  AlgoCast
//
//  Created by RickyoKita on 4/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import UIKit
import SafariServices
import CoreData
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  
  static let projectName = "AlgoCast"

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

      
      registerForRemoteNotification(application: application)
      
      let navigationBarAppearace = UINavigationBar.appearance()
      
      navigationBarAppearace.tintColor = uicolorFromHex(rgbValue: 0xffffff)
      navigationBarAppearace.barTintColor = uicolorFromHex(rgbValue: 0x6c6e7b)
      
      // change navigation item title color
      navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
      
     UIApplication.shared.statusBarStyle = .lightContent 
        // Override point for customization after application launch.
        
        // Check if launched from notification
        // 1
     /*   if let notification = (launchOptions as? [String: AnyObject])?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject] {
            // 2
            let aps = notification["aps"] as! [String: AnyObject]
            createNewNewsItem(aps)
            // 3
            if let tabController = window?.rootViewController as? UITabBarController {
                tabController.selectedIndex = 1
            }
        }*/
      

        
        return true
    }
  
  func uicolorFromHex(rgbValue:UInt32)->UIColor{
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    
    return UIColor(red:red, green:green, blue:blue, alpha:1.0)
  }
        
        // MARK: Helpers
    /*    func createNewNewsItem(notificationDictionary:[String: AnyObject]) -> PodcastItem? {
            if let news = notificationDictionary["alert"] as? String,
                let url = notificationDictionary["link_url"] as? String {
                let date = NSDate()
                
                let newsItem = PodcastItem(title: news, date: date, link: url)
                let newsStore = NewsStore.sharedStore
                newsStore.addItem(newsItem)
                
                NSNotificationCenter.defaultCenter().postNotificationName(NewsFeedTableViewController.RefreshNewsFeedNotification, object: self)
                return newsItem
            }
            return nil
        }*/
  
  func registerForRemoteNotification(application: UIApplication) {
    
    if #available(iOS 10.0, *) {
      
      let center  =  UNUserNotificationCenter.current()
      
      //center.delegate = self
      
      center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
        
        if error == nil{
          
          UIApplication.shared.registerForRemoteNotifications()
          
        }
        
      }
      
    }
      
    else {
      
      UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
      
      UIApplication.shared.registerForRemoteNotifications()
      
    }
    
  }
  
  
  
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      
      print("Failed to register:", error)
      
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      var token = ""
      for i in 0..<deviceToken.count {
        
        token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        
      }
      UserDefaults.standard.set(token, forKey: "deviceToken")
      print(token)
      
    }

    

    
  /*  // MARK: Register for push
    func registerForPushNotifications(application: UIApplication) {
        let viewAction = UIMutableUserNotificationAction()
        viewAction.identifier = "VIEW_IDENTIFIER"
        viewAction.title = "View"
        viewAction.activationMode = .foreground
        
        let newsCategory = UIMutableUserNotificationCategory()
        newsCategory.identifier = "NEWS_CATEGORY"
        newsCategory.setActions([viewAction], for: .default)
        
        let categories: Set<UIUserNotificationCategory> = [newsCategory]
        
        let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: categories)
        application.registerUserNotificationSettings(notificationSettings)
    }*/
  
  



    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
  
  func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask{
    return UIInterfaceOrientationMask.portrait
  }

  
  // MARK: - Core Data stack
  
  lazy var persistentContainer: NSPersistentContainer = {
    /*
     The persistent container for the application. This implementation
     creates and returns a container, having loaded the store for the
     application to it. This property is optional since there are legitimate
     error conditions that could cause the creation of the store to fail.
     */
    let container = NSPersistentContainer(name: "AlgoCast")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error as NSError? {
        // Replace this implementation with code to handle the error appropriately.
        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        
        /*
         Typical reasons for an error here include:
         * The parent directory does not exist, cannot be created, or disallows writing.
         * The persistent store is not accessible, due to permissions or data protection when the device is locked.
         * The device is out of space.
         * The store could not be migrated to the current model version.
         Check the error message to determine what the actual problem was.
         */
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })
    return container
  }()
  
  // MARK: - Core Data Saving support
  
  func saveContext () {
    let context = persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        // Replace this implementation with code to handle the error appropriately.
        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
  

  
}

