import Foundation
import Darwin

public class Connection : NSObject, StreamDelegate {
  static var serverAddress: CFString = Globals.remoteServer as CFString
  static var serverPort: UInt32 = UInt32(truncatingBitPattern: Int(Globals.remotePort as String)!)
  
  private var inputStream: InputStream!
  private var outputStream: OutputStream!
  
  func sendCommand(command : String) -> NSString{
    print("connecting...")
    
    var readStream:  Unmanaged<CFReadStream>?;
    var writeStream: Unmanaged<CFWriteStream>?;
    
    CFStreamCreatePairWithSocketToHost(nil, Connection.serverAddress, Connection.serverPort, &readStream, &writeStream)
    
    // Documentation suggests readStream and writeStream can be assumed to
    // be non-nil. If you believe otherwise, you can test if either is nil
    // and implement whatever error-handling you wish.
    
    self.inputStream = readStream!.takeRetainedValue()
    self.outputStream = writeStream!.takeRetainedValue()
    
    //veeko: delegate is disable for while loop checking
    // self.inputStream.delegate = self
    // self.outputStream.delegate = self
    
    self.inputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
    self.outputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
    
    self.inputStream.open()
    self.outputStream.open()
    
    while(true){
      if(self.outputStream.hasSpaceAvailable){
        let sendCommand = command+"\n"
        let encodedDataArray = [UInt8](sendCommand.utf8)
        self.outputStream.write(UnsafePointer<UInt8>(encodedDataArray), maxLength: encodedDataArray.count)
        break
      }
      
    }
    var buffer = [UInt8](repeating: 0, count: 40960)
    var finalBuffer = [UInt8](repeating: 0, count: 40960)
    var idx = 0;
    while(true){
      if( self.inputStream.hasBytesAvailable) {
          let len = inputStream.read(&buffer, maxLength: buffer.count)
          if(len > 0){
            for index:Int in 0  ..< len  {
              finalBuffer[idx+index]=buffer[index]
            }
            idx+=len
            //let output = NSString(bytes: &finalBuffer, length: finalBuffer.count, encoding: String.Encoding.utf8.rawValue)
            let output = NSString(bytes: &finalBuffer, length: idx, encoding: String.Encoding.utf8.rawValue)
          //server side will have the carrige return for line end signal
            if(output?.contains("\n"))!{
              print("!!!Server said: %@", output!,"\n")
              return NSString(bytes: &finalBuffer, length: idx-1, encoding: String.Encoding.utf8.rawValue)!

            }
          }
      }
    }
    return NSString(bytes: &buffer, length: buffer.count, encoding: String.Encoding.utf8.rawValue)!
  }
}
