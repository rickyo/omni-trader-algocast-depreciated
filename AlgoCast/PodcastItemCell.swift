//
//  PodcastItemCell.swift
//  AlgoCast
//
//  Created by RickyoKita on 10/10/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import UIKit

class PodcastItemCell: UITableViewCell {
  

    func updateWithPodcastItem(item:PodcastItem) {
        print(item.title)
        self.textLabel?.text = item.title
        self.detailTextLabel?.text = DateParser.displayString(fordate:item.publishedDate)
    }
}
