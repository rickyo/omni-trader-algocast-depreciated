//
//  Globals.swift
//  AlgoCast
//
//  Created by RickyoKita on 5/12/2016.
//  Copyright © 2016 Majestic. All rights reserved.
//

import Foundation
import UIKit

struct Globals
{
  static let ApplicationName : String = "Majestic Trader"
  
  static var loginName : String = ""
  static var loginPassword : String = ""
  
  static var remotePort : String = "5987"
  static var remoteServer : String = "veeko123.no-ip.org"
  
  static let cash : Double = 1259600
  
  static let serverTimeout : Int = 30
  
  static let marginHSI : Double = 71000
    static let marginMHI : Double = 14200
    static let marginHHI : Double =   30300
    static let marginMCH : Double =   6060
  
  static var currentProfit : Double = 23000
  static var currentNetgain : Int = 15
  
  static var currentTotalProfit : Double = 28000
  
  // tag
  static var tagBG:Int = 9999
  static var tagHeader:Int = 9998
  
  static var themeTextColor :UIColor = UIColor.white
  
}



class SharingManager {
  var pageTitle:String = ""
  
  var MCHnominalValue:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
  var MCHdayHigh : String = "0"
  var MCHdayLow : String = "0"
  var NetQty : String = "0"
  var theme : String = "DARK"
  
  static let sharedInstance = SharingManager()
}

class UserInfo {
  static  var userid = "1001085603"
  static var pwd = ""
}




class AppData: NSObject, NSCoding {
  
  
  let defaults = UserDefaults.standard
  
  
 // var remotePort : String = defaults.string(forKey: Globals.remotePort)!
  //var remoteServer : String = defaults.string(forKey: Globals.remoteServer)!
  
  required init(coder aDecoder: NSCoder) {
    super.init()
    self.loadData()

   
    
  }
  
  func encode(with aCoder: NSCoder) {   }
  
  func loadData() {
  }
  
  func saveDate(){
 //   defaults.setValue(remotePort, forKey: Globals.remotePort)
 //   defaults.setValue(remoteServer, forKey: Globals.remoteServer)
    
    defaults.synchronize()
  }
}
